package me.unknownteam.manhunt;

import java.util.logging.Logger;

import org.bukkit.plugin.java.JavaPlugin;

public class Manhunt extends JavaPlugin {

	public static Manhunt plugin;
	public static Logger log;
	
	public void onEnable(){
		plugin = this;
		log = this.getLogger();
		log.info(this + " has been enabled!");
	}
	
	public void onDisable(){
		plugin = null;
		log = null;
		log.info(this + " has been disabled!");
	}
	
}
